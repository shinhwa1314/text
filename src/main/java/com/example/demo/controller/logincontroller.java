package com.example.demo.controller;



import com.example.demo.dao.teacher;
import com.example.demo.dao.time;
import com.example.demo.dao.user;
import com.example.demo.interfacep.teacherApplication;
import com.example.demo.service.loginService;

import com.example.demo.interfacep.timeApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.web.bind.annotation.*;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

@RestController
public class logincontroller {
    @Autowired
    private timeApplication timeApplication;
    @Autowired
    private loginService loginService;
    @Autowired
    private teacherApplication teacherApplication;


    @PostMapping(value = "/login")

    public user login(@RequestBody()Map<String,String> map){

          return loginService.islogin(map.get("type"), map.get("name"), map.get("password"));
    }
    @GetMapping("/main")
    public teacher main(@RequestParam("id") Integer id){
        //在新的版本中将 T findOne(ID id);改为了更优雅的Optional<S> findOne(Example<S> example);即传参和回调分别改为了Example和Optional。因此方法使用与之前不同。
        teacher teacher=new teacher();
        teacher.setId(id);
        Example<teacher> teacherExample=Example.of(teacher);
        return teacherApplication.findOne(teacherExample).orElse(null);
    }
   @GetMapping(value = "/time")
   public Date deadline (@RequestBody()Map<String,String> map1)throws ParseException

    {
        //截止时间的设定是在每天的21；59；59秒
        String string = map1.get("year")+"-"+map1.get("month")+"-"+map1.get("day")+" 21:59:59";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date=sdf.parse(string);
        time time=new time();
        time.setTime(date);
        timeApplication.save(time);
        return time.getTime();

    }


}
